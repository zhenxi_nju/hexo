# 枕溪本纪
## Introduction
本项目由计科 & AI 的同学自发创建，意在收集 2021 年 5 月 16 日至 17 日资料群所发生事件的记录以及可能的前因后果。
乃至整个资料群逐渐向匿名群转化，再到最后名存实亡的资料纪录。

项目由 Hexo 驱动，部署于 Vercel，网页：[枕溪本纪](https://zhenxi.vercel.app)。

我们无意于迫害或是二次伤害，所以请谨慎上传资料。存在恶意引导倾向或是伪造证据的资料可能会被驳回。

## Installation
请确保电脑安装了 Node.js, Yarn 等工具来确保 Hexo 能够正常驱动。

Clone 下仓库后执行
```
yarn
```

如要新建文章，在命令行中执行
```
yarn hexo new post title-name
```

Hexo 会自动生成 `title-name.md` 以及同名的文件夹。在文件夹中存放 asset，然后在 Markdown 文件中直接引用即可。

## Contributing
务必保证发表的文章不会造成二次伤害。若有认为会有争议的内容，一律放在 `source/assets` 目录下。